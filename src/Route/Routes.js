import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import Home from 'src/Screens/Home/Home.component';
import Feed from 'src/Screens/Feed/Feed.component';

export default Routes = () => {
  return (
    <Router>
      <Scene key="root">
        <Scene
          key="Home"
          component={Home}
          hideNavBar={true}
          panHandlers={null}
          initial
        />
        <Scene
          key="Feed"
          component={Feed}
          hideNavBar={true}
          panHandlers={null}
        />
      </Scene>
    </Router>
  );
};

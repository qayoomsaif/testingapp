import {StyleSheet} from 'react-native';

export default styles = StyleSheet.create({
  mianBlock: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputText: {
    width: 300,
    height: 60,
    borderWidth: 1,
    padding: 10,
  },
});

import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';
import styles from './Home.style';
import Cotroller from './Home.controller';
import {Actions} from 'react-native-router-flux';

export default class Home extends React.Component {
  // constructor(props) {
  //   super(props);
  controller = new Cotroller(this);
  state = {
    email: 'saif@saif.saif',
    password: '123456',
  };
  // }

  goToNextScreen(email, passwrod) {
    if (!email) {
      console.log('Please enter email');
      return;
    }
    if (!passwrod) {
      console.log('Please enter password');
      return;
    }

    let validate = this.validateEmail(email);
    if (!validate) {
      console.log('Please enter a crroect email');
      return;
    }
    if (!(passwrod.length > 5)) {
      console.log('Please enter atleast 6 chartor password');
      return;
    }
    Actions.Feed({validEmail: email, validPassword: passwrod});

    // console.log(email, passwrod);
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,10}))$/;
    if (re.test(email)) {
      return true;
    }
    return false;
  }

  render() {
    return (
      <View style={styles.mianBlock}>
        <TextInput
          style={styles.inputText}
          onChangeText={(text) => this.setState({email: text})}
          value={this.state.email}
          placeholder={'Please enter a Email'}
          placeholderTextColor={'gray'}
          // style={styles.textInput}
        />
        <TextInput
          onChangeText={(text) => this.setState({password: text})}
          value={this.state.password}
          placeholder={'please enter a password'}
          placeholderTextColor={'gray'}
          style={styles.textInput}
        />
        <TouchableOpacity
          onPress={() =>
            this.controller.goToNextScreen(
              this.state.email,
              this.state.password,
            )
          }>
          <Text>GO</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

// export default class App extends React.Component {
//   render() {
//     return (
//       <View style={styles.mainBlock}>
//       <Text style ={styles.nameText} >qayoom</Text>
//       <Image
//       style ={styles.image}
//       source={require('src/assests/icons/logo.png')} />
//       </View>
//     );
//   }
// }

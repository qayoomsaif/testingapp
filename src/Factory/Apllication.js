import {Helper} from 'src/Manager';
// import {Router} from 'src/Route';
var applicationFactory = null;

class ApplicationFactory {
  constructor(config) {
      console.log(Helper);
    this.helper = new Helper();
  }

  /* When these below functions will be called from anywhere in the project they will call Manager functions  */
  getHelper() {
    return this.helper;
  }
}

/**
 * @param {*} config
 * @returns {ApplicationFactory}
 */
function getFactory(config) {
  if (!applicationFactory) {
    if (!config) {
      throw new Error('config not defined');
    }
    applicationFactory = new ApplicationFactory(config);
  }
  return applicationFactory;
}
module.exports = getFactory;

import {Actions} from 'react-native-router-flux';

export default class Router {
  async Home() {
    Actions.Home();
  }
}

// export default class Router {
//   constructor() {
//     this.lastScreen = null;
//   }
//   async stroeScreen(currentScene) {
//     this.lastScreen = Actions.currentScene;
//     this.currentScene = currentScene;
//   }
//   async activityDetailMainComponent(data) {
//     await this.stroeScreen('activityDetailMainComponent');
//     Actions.activityDetailMainComponent(data);
//   }

//   back(data) {
//     if (data) {
//       Actions.pop({refresh: {data, refreshs: new Date()}});
//       return;
//     }
//     Actions.pop();
//   }
//   Popdata(data) {
//     if (data) {
//       return {refresh: {data}};
//     }
//     return data;
//   }
//   backTo(name) {
//     Actions.popTo(name);
//   }
//   push(name, para) {
//     Actions.push(name, para);
//   }
//   refresh(para) {
//     Actions.refresh(para);
//   }
//   prevState() {
//     if (Actions.prevState.routes) {
//       return Actions.prevState.routes.map(function (i) {
//         return i.routeName;
//       });
//     }
//     return false;
//   }
//   screen() {
//     return Actions.currentScene;
//   }
//   state() {
//     if (Actions.state.routes) {
//       return Actions.state.routes.map(function (i) {
//         return i.routeName;
//       });
//     }
//     return false;
//   }
//   lastScene() {
//     return Actions.prevScene ? Actions.prevScene : false;
//   }
// }
module.exports = {Router};
